package online.epark.parkingapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import online.epark.parkingapp.controllers.AddParkingMapController
import android.content.pm.PackageManager
import com.otaliastudios.cameraview.CameraView
import online.epark.parkingapp.events.CameraAccessGrantedEvent
import online.epark.parkingapp.events.LocationAccessEvent
import online.epark.parkingapp.utils.LocationUtils
import org.greenrobot.eventbus.EventBus


class MainActivity : AppCompatActivity() {

    @BindView(R.id.controller_container) lateinit var container : ViewGroup
    @BindView(R.id.toolbar) lateinit var toolbar : Toolbar

    lateinit var router : Router

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }


        router = Conductor.attachRouter(this, container, savedInstanceState)
        if (!router.hasRootController()) {
            router.setRoot(RouterTransaction.with(AddParkingMapController()))
        }
    }

    override fun onBackPressed() {
        if (!router.handleBack()) {
            super.onBackPressed()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            LocationUtils.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                var granted = false
                if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    granted = true
                }
                EventBus.getDefault().post(LocationAccessEvent(granted))
            }
            CameraView.PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    EventBus.getDefault().post(CameraAccessGrantedEvent())
                }
            }
        }
    }
}
