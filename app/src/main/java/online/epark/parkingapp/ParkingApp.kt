package online.epark.parkingapp

import android.app.Application
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher
import online.epark.parkingapp.events.LowMemoryEvent
import org.greenrobot.eventbus.EventBus

/**
 *
 * @author Andrey Podgorodetskiy
 */
 

class ParkingApp : Application() {

    companion object {
        lateinit var refWatcher: RefWatcher
    }

    override fun onCreate() {
        super.onCreate()
        refWatcher = LeakCanary.install(this)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        EventBus.getDefault().post(LowMemoryEvent())
    }
}