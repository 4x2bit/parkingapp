package online.epark.parkingapp.controllers

import android.os.Bundle
import android.support.v7.widget.AppCompatEditText
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import butterknife.BindView
import butterknife.OnClick
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import online.epark.parkingapp.R
import online.epark.parkingapp.controllers.base.BaseController
import online.epark.parkingapp.models.Parking
import online.epark.parkingapp.service.ServiceClient
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.slots.PredefinedSlots
import ru.tinkoff.decoro.watchers.MaskFormatWatcher


/**
 *
 * @author Andrey Podgorodetskiy
 */
 

class AddParkingFinalStepController : BaseController {

    lateinit var parking: Parking

    constructor(bundle: Bundle) : super(bundle)
    constructor(parking: Parking){
        this.parking = parking
    }

    @BindView(R.id.phone)
    lateinit var phone : AppCompatEditText

    @BindView(R.id.comment)
    lateinit var comment : AppCompatEditText

    @BindView(R.id.progressBarLayout)
    lateinit var progressBarLayout : FrameLayout

    override fun inflateView(inflater: LayoutInflater, container: ViewGroup): View {
        return inflater.inflate(R.layout.controller_add_parking_final_step, container, false)
    }

    override fun onViewBound(view: View) {
        val mask = MaskImpl.createTerminated(PredefinedSlots.RUS_PHONE_NUMBER)
        mask.placeholder = '_'
        mask.isShowingEmptySlots = true
        val watcher = MaskFormatWatcher(mask)
        watcher.installOn(phone)
    }

    @OnClick(R.id.sendButton)
    fun sendClick() {
        if(phone.text.toString().length < 18) {
            Toast.makeText(activity, R.string.conroller_apfs_fill_phone_error,
                    Toast.LENGTH_LONG).show()
            return
        }
        hideKeyboard()
        showProgress()
        ServiceClient.createParking(
                phone.text.toString(),
                comment.text.toString(),
                parking.address!!.latitude,
                parking.address!!.longitude,
                parking.address!!.countryCode,
                parking.address!!.countryName,
                parking.address!!.locality,
                parking.address!!.postalCode,
                parking.address!!.thoroughfare,
                parking.address!!.featureName,
                parking.picture!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            hideProgress()
                            router.popToRoot()
                            Toast.makeText(activity, R.string.conroller_apfs_success,
                                    Toast.LENGTH_SHORT).show()
                        },
                        { error ->
                            Toast.makeText(activity, error.message, Toast.LENGTH_SHORT).show()
                            hideProgress()
                        }
                )
    }

    fun showProgress() {
        progressBarLayout.visibility = View.VISIBLE
    }

    fun hideProgress() {
        progressBarLayout.visibility = View.GONE
    }
}
