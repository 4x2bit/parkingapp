package online.epark.parkingapp.controllers

import android.annotation.SuppressLint
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.OnClick
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import online.epark.parkingapp.R
import online.epark.parkingapp.controllers.common.MapController
import online.epark.parkingapp.events.LocationAccessEvent
import online.epark.parkingapp.models.Parking
import online.epark.parkingapp.utils.LocationUtils
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

/**
 *
 * @author Andrey Podgorodetskiy
 */
 

class AddParkingMapController : MapController, GoogleMap.OnMapClickListener,
        GoogleMap.OnCameraIdleListener {

    constructor()
    constructor(bundle: Bundle) : super(bundle)

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    var lastCameraPostition : LatLng = LatLng(0.0, 0.0)
    var address : Address? = null

    @BindView(R.id.mapView)
    override lateinit var mapView : MapView

    @BindView(R.id.mapLayout)
    lateinit var mapLayout : FrameLayout

    @BindView(R.id.locationPermissionsLayout)
    lateinit var locationPermissionLayout : FrameLayout

    @BindView(R.id.address)
    lateinit var addressTextView : TextView

    override fun inflateView(inflater: LayoutInflater, container: ViewGroup): View {
        retainViewMode = Controller.RetainViewMode.RETAIN_DETACH
        return inflater.inflate(R.layout.controller_add_parking_map, container, false);
    }

    override fun onViewBound(view: View) {
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        super.onMapReady(googleMap)
        if (googleMap == null) {
            return
        }

        if (LocationUtils.isLocationEnabled(applicationContext!!)) {
            setupMap()
        } else {
            LocationUtils.getLocationPermission(activity)
        }
    }

    @OnClick(R.id.locationPermissionsButton)
    fun locationPermissionClick() {
        LocationUtils.getLocationPermission(activity)
    }

    @OnClick(R.id.confirmButton)
    fun confirmClick() {
        if(address == null) {
            AlertDialog.Builder(activity!!)
                    .setMessage(R.string.address_dialog_message)
                    .setTitle(R.string.address_dialog_title)
                    .setPositiveButton(R.string.location_dialog_ok_title) {
                        _, _ ->
                    }
                    .create()
                    .show()
            return
        }
        var parking = Parking(address, null, null)
        router.pushController(RouterTransaction
                .with(AddParkingPhotoController(parking))
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLocationAccessEvent(event: LocationAccessEvent) {
        if(event.hasAccess) {
            showMapScreen()
            setupMap()
        } else {
            showPermissionsPopup()
        }
    }

    private fun showPermissionsPopup() {
        AlertDialog.Builder(activity!!)
                .setMessage(R.string.location_dialog_message)
                .setTitle(R.string.location_dialog_title)
                .setPositiveButton(R.string.location_dialog_ok_title) {
                    _, _ -> LocationUtils.getLocationPermission(activity)
                }
                .setNegativeButton(R.string.location_dialog_cancel_title) {
                    p0, _ ->
                    p0.dismiss()
                    showLocationScreen()
                }
                .create()
                .show()
    }

    private fun showLocationScreen() {
        mapLayout.visibility = View.GONE
        locationPermissionLayout.visibility = View.VISIBLE
    }

    private fun showMapScreen() {
        mapLayout.visibility = View.VISIBLE
        locationPermissionLayout.visibility = View.GONE
    }

    @SuppressLint("MissingPermission")
    private fun setupMap() {
        googleMap.setOnMapClickListener(null)
        googleMap.setOnMapClickListener(this)
        googleMap.setOnCameraIdleListener(null)
        googleMap.setOnCameraIdleListener(this)
        googleMap.isMyLocationEnabled = true

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)
        fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    if(location == null) {
                        return@addOnSuccessListener
                    }
                    var latLng = LatLng(location.latitude, location.longitude)
                    var cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17f)
                    googleMap.animateCamera(cameraUpdate)
                }
    }

    override fun onCameraIdle() {
        if (lastCameraPostition != googleMap.cameraPosition.target) {
            lastCameraPostition = googleMap.cameraPosition.target
            val lastCameraPostition = googleMap.cameraPosition.target
            val geocoder = Geocoder(activity, Locale.getDefault())
            val address = geocoder.getFromLocation(lastCameraPostition.latitude,
                    lastCameraPostition.longitude,1).first()
            if (address != null) {
                addressTextView.text = address.getAddressLine(0)
                        .substringBefore(address.locality)
                        .substringBeforeLast(",")
                this.address = address
            } else {
                addressTextView.text = "N/A"
                this.address = null
            }
        }
    }

    override fun onMapClick(latLng: LatLng?) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng))
    }

}