package online.epark.parkingapp.controllers

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import butterknife.BindView
import butterknife.OnClick
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.otaliastudios.cameraview.*
import online.epark.parkingapp.R
import online.epark.parkingapp.controllers.base.BaseController
import online.epark.parkingapp.events.CameraAccessGrantedEvent
import online.epark.parkingapp.models.Parking
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


/**
 *
 * @author Andrey Podgorodetskiy
 */
 

class AddParkingPhotoController : BaseController {

    lateinit var parking: Parking
    lateinit var picture: ByteArray
    lateinit var bitmap: Bitmap

    constructor(bundle: Bundle) : super(bundle)
    constructor(parking: Parking) {
        this.parking = parking
    }

    @BindView(R.id.camera)
    lateinit var cameraView: CameraView


    @BindView(R.id.cameraLayout)
    lateinit var cameraLayout: FrameLayout

    @BindView(R.id.cameraPreview)
    lateinit var cameraPreview: FrameLayout

    @BindView(R.id.cameraPreviewImageView)
    lateinit var cameraPreviewImageView: ImageView

    override fun inflateView(inflater: LayoutInflater, container: ViewGroup): View {
        retainViewMode = Controller.RetainViewMode.RETAIN_DETACH
        return inflater.inflate(R.layout.controller_parking_photo, container, false)
    }

    @OnClick(R.id.capturePhoto)
    fun capturePhoto(){
        cameraView.capturePicture()
    }

    @OnClick(R.id.recapturePhoto)
    fun recapturePhotoClick(){
        cameraPreview.visibility = View.GONE
        cameraLayout.visibility = View.VISIBLE
    }

    @OnClick(R.id.nextStep)
    fun nextStpClick(){
        parking.bitmap = bitmap
        parking.picture = picture

        router.pushController(RouterTransaction
                .with(AddParkingFinalStepController(parking))
                .pushChangeHandler(FadeChangeHandler())
                .popChangeHandler(FadeChangeHandler()))
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onCameraAccessGrantedEvent(event: CameraAccessGrantedEvent) {
        cameraView.start()
    }

    override fun onViewBound(view: View) {
        cameraView.sessionType = SessionType.PICTURE
        cameraView.jpegQuality = 70
        cameraView.mapGesture(Gesture.PINCH, GestureAction.ZOOM)
        cameraView.mapGesture(Gesture.TAP, GestureAction.FOCUS_WITH_MARKER)
        cameraView.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(jpeg: ByteArray?) {
                if(jpeg == null) {
                    return
                }
                picture = jpeg
                CameraUtils.decodeBitmap(jpeg) {
                    bmp ->
                    cameraPreview.visibility = View.VISIBLE
                    cameraLayout.visibility = View.GONE
                    cameraPreviewImageView.setImageBitmap(bmp)
                    bitmap = bmp
                }
            }
        })
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        cameraView.start()
        EventBus.getDefault().register(this)
    }

    override fun onDetach(view: View) {
        super.onDetach(view)
        cameraView.stop()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroyView(view: View) {
        cameraView.destroy()
        super.onDestroyView(view)
    }
}