package online.epark.parkingapp.controllers.base

import android.os.Bundle
import android.view.View
import online.epark.parkingapp.MainActivity
import online.epark.parkingapp.R
import android.app.Activity
import android.view.inputmethod.InputMethodManager


/**
 *
 * @author Andrey Podgorodetskiy
 */


abstract class BaseController : RefWatchingController {

    constructor()
    constructor(bundle: Bundle) : super(bundle)

    override fun onAttach(view: View) {
        super.onAttach(view)

        val activity = (activity as MainActivity?)!!

        activity.enableUpArrow(router.backstackSize > 1)
    }

    fun hideKeyboard() {
        val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity?.getCurrentFocus()
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm?.hideSoftInputFromWindow(view.windowToken, 0)
    }

}

    private fun MainActivity.enableUpArrow(enabled: Boolean) {
    if (enabled) {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
    } else {
        toolbar.navigationIcon = null
    }
}
