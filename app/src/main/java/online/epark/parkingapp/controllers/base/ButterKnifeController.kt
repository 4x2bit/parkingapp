package online.epark.parkingapp.controllers.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import butterknife.Unbinder
import com.bluelinelabs.conductor.Controller

/**
 *
 * @author Andrey Podgorodetskiy
 */

abstract class ButterKnifeController : Controller {
    private var unBinder: Unbinder? = null

    constructor() {
    }

    constructor(bundle: Bundle) : super(bundle) {
    }

    protected abstract fun inflateView(inflater: LayoutInflater, container: ViewGroup): View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val view = inflateView(inflater, container)
        unBinder = ButterKnife.bind(this, view)
        onViewBound(view)
        return view
    }

    abstract fun onViewBound(view: View)

    override fun onDestroyView(view: View) {
        super.onDestroyView(view)
        unBinder!!.unbind()
        unBinder = null
    }
}