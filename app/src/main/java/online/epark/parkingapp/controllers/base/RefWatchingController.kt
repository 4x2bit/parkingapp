package online.epark.parkingapp.controllers.base

import android.os.Bundle
import com.bluelinelabs.conductor.ControllerChangeHandler
import com.bluelinelabs.conductor.ControllerChangeType
import online.epark.parkingapp.ParkingApp

/**
 *
 * @author Andrey Podgorodetskiy
 */


abstract class RefWatchingController : ButterKnifeController {
    constructor()
    constructor(bundle: Bundle) : super(bundle)

    private var hasExited: Boolean = false

    public override fun onDestroy() {
        super.onDestroy()

        if (hasExited) {
            ParkingApp.refWatcher.watch(this)
        }
    }

    override fun onChangeEnded(changeHandler: ControllerChangeHandler, changeType: ControllerChangeType) {
        super.onChangeEnded(changeHandler, changeType)

        hasExited = !changeType.isEnter
        if (isDestroyed) {
            ParkingApp.refWatcher.watch(this)
        }
    }
}