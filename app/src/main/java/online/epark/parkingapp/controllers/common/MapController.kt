package online.epark.parkingapp.controllers.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import online.epark.parkingapp.controllers.base.BaseController
import online.epark.parkingapp.events.LowMemoryEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 *
 * @author Andrey Podgorodetskiy
 */


abstract class MapController : BaseController, OnMapReadyCallback {

    constructor()
    constructor(bundle: Bundle) : super(bundle) {
        this.bundle = bundle;
    }

    var bundle : Bundle? = null
    lateinit var googleMap : GoogleMap
    open lateinit var mapView : MapView

    override fun onMapReady(googleMap: GoogleMap?) {
        if(googleMap!= null) {
            this.googleMap = googleMap;
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        EventBus.getDefault().register(this);
        var view = super.onCreateView(inflater, container)
        mapView.onCreate(bundle)
        mapView.getMapAsync(this)
        return view;
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        mapView.onResume()
    }

    override fun onDetach(view: View) {
        super.onDetach(view)
        mapView.onPause()
    }

    override fun onDestroyView(view: View) {
        mapView.onDestroy()
        EventBus.getDefault().unregister(this);
        super.onDestroyView(view)
    }

    override fun onSaveViewState(view: View, outState: Bundle) {
        super.onSaveViewState(view, outState)
        mapView.onSaveInstanceState(bundle)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLowMemoryEvent(event: LowMemoryEvent) {
        mapView.onLowMemory()
    }

}