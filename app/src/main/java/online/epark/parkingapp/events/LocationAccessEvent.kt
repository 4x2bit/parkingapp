package online.epark.parkingapp.events

/**
 *
 * @author Andrey Podgorodetskiy
 */
 

class LocationAccessEvent(hasAccess: Boolean) {
    var hasAccess = hasAccess;
}