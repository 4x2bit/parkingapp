package online.epark.parkingapp.models

import android.graphics.Bitmap
import android.location.Address

/**
 *
 * @author Andrey Podgorodetskiy
 */


data class Parking(var address: Address?, var picture: ByteArray?, var bitmap: Bitmap?)