package online.epark.parkingapp.service

import io.reactivex.Observable
import okhttp3.Call
import okhttp3.MultipartBody
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.POST
import retrofit2.http.Multipart
import retrofit2.http.Part
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor




/**
 *
 * @author Andrey Podgorodetskiy
 */


interface ParkingApi {
    @Multipart
    @POST("create/")
    fun createParking(
            @Part phone: MultipartBody.Part,
            @Part comment: MultipartBody.Part,
            @Part latitude: MultipartBody.Part,
            @Part longitude: MultipartBody.Part,
            @Part countryCode: MultipartBody.Part,
            @Part countryName: MultipartBody.Part,
            @Part city: MultipartBody.Part,
            @Part postalCode: MultipartBody.Part,
            @Part street: MultipartBody.Part,
            @Part building: MultipartBody.Part,
            @Part photo: MultipartBody.Part): Observable<Model.Result>

    companion object Factory {
        fun create(): ParkingApi {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://159.65.121.144:8000/api/")
                    .client(httpClient.build())
                    .build()

            return retrofit.create(ParkingApi::class.java);
        }
    }

}