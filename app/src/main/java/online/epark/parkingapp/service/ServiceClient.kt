package online.epark.parkingapp.service

import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

/**
 *
 * @author Andrey Podgorodetskiy
 */


object ServiceClient {
    private val apiService by lazy {
        ParkingApi.create()
    }

    fun createParking(phone: String,
                      comment: String,
                      latitude: Double,
                      longitude: Double,
                      countryCode: String,
                      countryName: String,
                      city: String,
                      postalCode: String,
                      street: String,
                      building: String,
                      photo: ByteArray): Observable<Model.Result> {
        var decimalFormatSymbols = DecimalFormatSymbols(Locale.US)
        var decimalFormat = DecimalFormat("##.######", decimalFormatSymbols);
        val phonePart = MultipartBody.Part.createFormData("phone", phone)
        val comment = MultipartBody.Part.createFormData("comment", comment)
        val latitude = MultipartBody.Part.createFormData("latitude", decimalFormat.format(latitude))
        val longitude = MultipartBody.Part.createFormData("longitude", decimalFormat.format(longitude))
        val countryCode = MultipartBody.Part.createFormData("countryCode", countryCode)
        val countryName = MultipartBody.Part.createFormData("countryName", countryName)
        val city = MultipartBody.Part.createFormData("city", city)
        val postalCode = MultipartBody.Part.createFormData("postalCode", postalCode)
        val street = MultipartBody.Part.createFormData("street", street)
        val building = MultipartBody.Part.createFormData("building", building)
        val photoPart = MultipartBody.Part.createFormData("photo", "photo"+Date(),
                RequestBody.create(MediaType.parse("image/*"), photo))
        return apiService.createParking(phonePart, comment, latitude, longitude, countryCode,
                countryName, city, postalCode, street, building, photoPart)
    }
}