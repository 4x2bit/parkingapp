package online.epark.parkingapp.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat


/**
 *
 * @author Andrey Podgorodetskiy
 */
 

object LocationUtils {
    const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 4666

    fun isLocationEnabled(context: Context): Boolean {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED)
    }

    fun getLocationPermission(activity: Activity?) {
        if (activity == null) {
            return
        }
        ActivityCompat.requestPermissions(activity,
                arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
    }
}